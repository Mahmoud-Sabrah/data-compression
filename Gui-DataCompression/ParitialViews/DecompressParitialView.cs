﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using CompressAlgorithms.Lossless.Lzw;
using CompressAlgorithms.Lossless.Huffman;

namespace Gui_DataCompression.ParitialViews
{
    public partial class DecompressParitialView : UserControl
    {
        public DecompressParitialView()
        {
            InitializeComponent();

            openFileDialog1.Filter = "Huffman Compressed (*.cfano)|*.cfano;|LZW Compressed (*.clzw)|*.clzw;";
        }

        private void lst_files_DragDrop(object sender, DragEventArgs e)
        {
            string[] dropped_name = (string[])e.Data.GetData(DataFormats.FileDrop, false);


            string[] details = new string[4];
            FileInfo f_info = null;
            ListViewItem ls = null;

            details[2] = "-";
            details[3] = "-";

            for (int i = 0; i < dropped_name.Length; i++)
            {
                if (Path.GetExtension(dropped_name[i]) != "cfano" && Path.GetExtension(dropped_name[i]) != "clzw")
                    continue;
                f_info = new FileInfo(dropped_name[i]);
                details[0] = dropped_name[i];
                details[1] = f_info.Length.ToString();
                ls = new ListViewItem(details);
                lst_files.Items.Add(ls);
                lst_files.Items[lst_files.Items.Count - 1].Checked = true;
            }

        }

        private void lst_files_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void btn_addfile_Click(object sender, EventArgs e)
        {
            var result = openFileDialog1.ShowDialog();
            if (result != DialogResult.OK)
                return;

            FileInfo f_info = null;
            foreach (var item in openFileDialog1.FileNames)
            {
                f_info = new FileInfo(item);
                var lst = new ListViewItem(new string[] { item, f_info.Length.ToString(), "-", "-" });
                lst.Checked = true;
                lst_files.Items.Add(lst);
            }

        }

        private void lst_files_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (lst_files.SelectedItems.Count == 0) return;
                int before_count = lst_files.SelectedItems.Count;
                for (int i = 0; i < before_count; i++)
                    lst_files.Items.RemoveAt(lst_files.SelectedItems[0].Index);
            }
        }

        private void btn_compress_Click(object sender, EventArgs e)
        {

            var uiThreadScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        
            var items_count = lst_files.Items.Count;
            if (items_count == 0 )
                return;


            var compressTask = Task.Run(() => {
                for (int j = 0; j < items_count; j++)
                {
                    string fileName = string.Empty;
                    string fileSize = string.Empty;
                    string destinationFileName = string.Empty;
                    bool used = false;

                    Invoke(new Action(() =>
                    {
                        if (lst_files.Items[j].SubItems[2].Text != "-" || lst_files.Items[j].Checked == false)
                            used = false;
                        else
                        {
                            fileName = lst_files.Items[j].SubItems[0].Text;
                            fileSize = lst_files.Items[j].SubItems[1].Text;
                            used = true;

                            progressBar1.Style = ProgressBarStyle.Marquee;
                        }
                               
                    }));

                    if (!used)
                        continue;


                    destinationFileName = Path.Combine(Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName));
                    destinationFileName += Decompress(fileName, destinationFileName);


                    Invoke(new Action(() =>
                    {
                        fileName = lst_files.Items[j].SubItems[0].Text;
                        fileSize = lst_files.Items[j].SubItems[1].Text;

                        ListViewItem lst = lst_files.Items[j];
                        lst.SubItems[2].Text = destinationFileName;

                        FileInfo ff = new FileInfo(destinationFileName);
                        lst.SubItems[3].Text = ff.Length.ToString();

                    }));
                }
                }).ContinueWith((t) => {
                progressBar1.Style = ProgressBarStyle.Continuous;
            }, uiThreadScheduler);

          
    
        }





        string Decompress(string fileName, string destinationFileName)
        {

            if (Path.GetExtension(fileName) == ".clzw")
            {
                destinationFileName = destinationFileName + ".dlzw";
                lzwAlgo lzw = new lzwAlgo();
                lzw.Decompress(fileName, destinationFileName);
                return ".dlzw";
            }
            else if (Path.GetExtension(fileName) == ".cfano")
            {
                destinationFileName = destinationFileName + ".dfano";
                Huffman huf = new Huffman();
                huf.Decompress(fileName, destinationFileName);
                return ".dfano";
            }
            return null;
   
        }


    }
}
