﻿namespace Gui_DataCompression.ParitialViews
{
    partial class DecompressParitialView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DecompressParitialView));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btn_addfile = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btn_compress = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lst_files = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.BackColor = System.Drawing.Color.Silver;
            this.progressBar1.Location = new System.Drawing.Point(14, 296);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(632, 35);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 10;
            // 
            // btn_addfile
            // 
            this.btn_addfile.ActiveBorderThickness = 1;
            this.btn_addfile.ActiveCornerRadius = 10;
            this.btn_addfile.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_addfile.ActiveForecolor = System.Drawing.Color.Transparent;
            this.btn_addfile.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_addfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addfile.BackColor = System.Drawing.SystemColors.Control;
            this.btn_addfile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_addfile.BackgroundImage")));
            this.btn_addfile.ButtonText = "Add Files";
            this.btn_addfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_addfile.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_addfile.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_addfile.IdleBorderThickness = 1;
            this.btn_addfile.IdleCornerRadius = 10;
            this.btn_addfile.IdleFillColor = System.Drawing.Color.Transparent;
            this.btn_addfile.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_addfile.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_addfile.Location = new System.Drawing.Point(654, 294);
            this.btn_addfile.Margin = new System.Windows.Forms.Padding(5);
            this.btn_addfile.Name = "btn_addfile";
            this.btn_addfile.Size = new System.Drawing.Size(181, 41);
            this.btn_addfile.TabIndex = 9;
            this.btn_addfile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_addfile.Click += new System.EventHandler(this.btn_addfile_Click);
            // 
            // btn_compress
            // 
            this.btn_compress.ActiveBorderThickness = 1;
            this.btn_compress.ActiveCornerRadius = 10;
            this.btn_compress.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_compress.ActiveForecolor = System.Drawing.Color.Transparent;
            this.btn_compress.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_compress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_compress.BackColor = System.Drawing.SystemColors.Control;
            this.btn_compress.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_compress.BackgroundImage")));
            this.btn_compress.ButtonText = "Decompress";
            this.btn_compress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_compress.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_compress.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_compress.IdleBorderThickness = 1;
            this.btn_compress.IdleCornerRadius = 10;
            this.btn_compress.IdleFillColor = System.Drawing.Color.Transparent;
            this.btn_compress.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_compress.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_compress.Location = new System.Drawing.Point(14, 339);
            this.btn_compress.Margin = new System.Windows.Forms.Padding(5);
            this.btn_compress.Name = "btn_compress";
            this.btn_compress.Size = new System.Drawing.Size(181, 41);
            this.btn_compress.TabIndex = 9;
            this.btn_compress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_compress.Click += new System.EventHandler(this.btn_compress_Click);
            // 
            // lst_files
            // 
            this.lst_files.AllowDrop = true;
            this.lst_files.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lst_files.CheckBoxes = true;
            this.lst_files.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lst_files.FullRowSelect = true;
            this.lst_files.GridLines = true;
            this.lst_files.HideSelection = false;
            this.lst_files.Location = new System.Drawing.Point(14, 18);
            this.lst_files.Name = "lst_files";
            this.lst_files.ShowItemToolTips = true;
            this.lst_files.Size = new System.Drawing.Size(817, 253);
            this.lst_files.TabIndex = 12;
            this.lst_files.UseCompatibleStateImageBehavior = false;
            this.lst_files.View = System.Windows.Forms.View.Details;
            this.lst_files.DragDrop += new System.Windows.Forms.DragEventHandler(this.lst_files_DragDrop);
            this.lst_files.DragEnter += new System.Windows.Forms.DragEventHandler(this.lst_files_DragEnter);
            this.lst_files.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lst_files_KeyUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "File Name";
            this.columnHeader1.Width = 216;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Orginal File Size (Bytes)";
            this.columnHeader2.Width = 127;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Output File";
            this.columnHeader3.Width = 341;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Output File Size (bytes)";
            this.columnHeader4.Width = 125;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // DecompressParitialView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lst_files);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btn_compress);
            this.Controls.Add(this.btn_addfile);
            this.Name = "DecompressParitialView";
            this.Size = new System.Drawing.Size(851, 394);
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_addfile;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_compress;
        private System.Windows.Forms.ListView lst_files;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
