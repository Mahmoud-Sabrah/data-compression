﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;

namespace Gui_DataCompression.ParitialViews
{
    public partial class ImageViewerParitialView : UserControl
    {
        public ImageViewerParitialView()
        {
            InitializeComponent();

            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            var result = openFileDialog1.ShowDialog();
            if (result != DialogResult.OK)
                return;


            pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image.Save(@"test.jpg", ImageFormat.Jpeg);
        }
        private void txt_path_TextChanged(object sender, EventArgs e)
        {            
    
        }

    }
}
