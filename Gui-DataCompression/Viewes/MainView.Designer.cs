﻿namespace Gui_DataCompression.Viewes
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pnl_toolbar = new System.Windows.Forms.Panel();
            this.btn_min = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_max = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_close = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnl_slide = new System.Windows.Forms.Panel();
            this.btn_viewer = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_viewdecompress = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_viewcompress = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_slidemenu = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnl_container = new System.Windows.Forms.Panel();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.pnl_toolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.pnl_slide.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_slidemenu)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // pnl_toolbar
            // 
            this.pnl_toolbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.pnl_toolbar.Controls.Add(this.btn_min);
            this.pnl_toolbar.Controls.Add(this.btn_max);
            this.pnl_toolbar.Controls.Add(this.btn_close);
            this.pnl_toolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_toolbar.Location = new System.Drawing.Point(42, 0);
            this.pnl_toolbar.Name = "pnl_toolbar";
            this.pnl_toolbar.Size = new System.Drawing.Size(940, 48);
            this.pnl_toolbar.TabIndex = 2;
            this.pnl_toolbar.DoubleClick += new System.EventHandler(this.pnl_toolbar_DoubleClick);
            // 
            // btn_min
            // 
            this.btn_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_min.BackColor = System.Drawing.Color.Transparent;
            this.btn_min.Image = ((System.Drawing.Image)(resources.GetObject("btn_min.Image")));
            this.btn_min.ImageActive = null;
            this.btn_min.Location = new System.Drawing.Point(826, 9);
            this.btn_min.Name = "btn_min";
            this.btn_min.Size = new System.Drawing.Size(30, 30);
            this.btn_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_min.TabIndex = 0;
            this.btn_min.TabStop = false;
            this.btn_min.Zoom = 10;
            this.btn_min.Click += new System.EventHandler(this.btn_min_Click);
            // 
            // btn_max
            // 
            this.btn_max.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_max.BackColor = System.Drawing.Color.Transparent;
            this.btn_max.Image = ((System.Drawing.Image)(resources.GetObject("btn_max.Image")));
            this.btn_max.ImageActive = null;
            this.btn_max.Location = new System.Drawing.Point(862, 9);
            this.btn_max.Name = "btn_max";
            this.btn_max.Size = new System.Drawing.Size(30, 30);
            this.btn_max.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_max.TabIndex = 0;
            this.btn_max.TabStop = false;
            this.btn_max.Zoom = 10;
            this.btn_max.Click += new System.EventHandler(this.btn_max_Click);
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.BackColor = System.Drawing.Color.Transparent;
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.ImageActive = null;
            this.btn_close.Location = new System.Drawing.Point(898, 9);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(30, 30);
            this.btn_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_close.TabIndex = 0;
            this.btn_close.TabStop = false;
            this.btn_close.Zoom = 10;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // pnl_slide
            // 
            this.pnl_slide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.pnl_slide.Controls.Add(this.btn_viewer);
            this.pnl_slide.Controls.Add(this.btn_viewdecompress);
            this.pnl_slide.Controls.Add(this.btn_viewcompress);
            this.pnl_slide.Controls.Add(this.btn_slidemenu);
            this.pnl_slide.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_slide.Location = new System.Drawing.Point(0, 0);
            this.pnl_slide.Name = "pnl_slide";
            this.pnl_slide.Size = new System.Drawing.Size(42, 652);
            this.pnl_slide.TabIndex = 3;
            // 
            // btn_viewer
            // 
            this.btn_viewer.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(103)))), ((int)(((byte)(158)))));
            this.btn_viewer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_viewer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_viewer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_viewer.BorderRadius = 0;
            this.btn_viewer.ButtonText = "Jpeg Compress";
            this.btn_viewer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_viewer.DisabledColor = System.Drawing.Color.Gray;
            this.btn_viewer.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_viewer.Iconimage = null;
            this.btn_viewer.Iconimage_right = null;
            this.btn_viewer.Iconimage_right_Selected = null;
            this.btn_viewer.Iconimage_Selected = null;
            this.btn_viewer.IconMarginLeft = 0;
            this.btn_viewer.IconMarginRight = 0;
            this.btn_viewer.IconRightVisible = true;
            this.btn_viewer.IconRightZoom = 0D;
            this.btn_viewer.IconVisible = true;
            this.btn_viewer.IconZoom = 90D;
            this.btn_viewer.IsTab = false;
            this.btn_viewer.Location = new System.Drawing.Point(-148, 181);
            this.btn_viewer.Name = "btn_viewer";
            this.btn_viewer.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_viewer.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(103)))), ((int)(((byte)(158)))));
            this.btn_viewer.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_viewer.selected = false;
            this.btn_viewer.Size = new System.Drawing.Size(187, 48);
            this.btn_viewer.TabIndex = 1;
            this.btn_viewer.Text = "Jpeg Compress";
            this.btn_viewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_viewer.Textcolor = System.Drawing.Color.White;
            this.btn_viewer.TextFont = new System.Drawing.Font("29LT Bukra Regular", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_viewer.Click += new System.EventHandler(this.btn_viewer_Click);
            // 
            // btn_viewdecompress
            // 
            this.btn_viewdecompress.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(103)))), ((int)(((byte)(158)))));
            this.btn_viewdecompress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_viewdecompress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_viewdecompress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_viewdecompress.BorderRadius = 0;
            this.btn_viewdecompress.ButtonText = "Decompress";
            this.btn_viewdecompress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_viewdecompress.DisabledColor = System.Drawing.Color.Gray;
            this.btn_viewdecompress.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_viewdecompress.Iconimage = null;
            this.btn_viewdecompress.Iconimage_right = null;
            this.btn_viewdecompress.Iconimage_right_Selected = null;
            this.btn_viewdecompress.Iconimage_Selected = null;
            this.btn_viewdecompress.IconMarginLeft = 0;
            this.btn_viewdecompress.IconMarginRight = 0;
            this.btn_viewdecompress.IconRightVisible = true;
            this.btn_viewdecompress.IconRightZoom = 0D;
            this.btn_viewdecompress.IconVisible = true;
            this.btn_viewdecompress.IconZoom = 90D;
            this.btn_viewdecompress.IsTab = false;
            this.btn_viewdecompress.Location = new System.Drawing.Point(-148, 127);
            this.btn_viewdecompress.Name = "btn_viewdecompress";
            this.btn_viewdecompress.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_viewdecompress.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(103)))), ((int)(((byte)(158)))));
            this.btn_viewdecompress.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_viewdecompress.selected = false;
            this.btn_viewdecompress.Size = new System.Drawing.Size(187, 48);
            this.btn_viewdecompress.TabIndex = 1;
            this.btn_viewdecompress.Text = "Decompress";
            this.btn_viewdecompress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_viewdecompress.Textcolor = System.Drawing.Color.White;
            this.btn_viewdecompress.TextFont = new System.Drawing.Font("29LT Bukra Regular", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_viewdecompress.Click += new System.EventHandler(this.btn_viewdecompress_Click);
            // 
            // btn_viewcompress
            // 
            this.btn_viewcompress.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(103)))), ((int)(((byte)(158)))));
            this.btn_viewcompress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_viewcompress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_viewcompress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_viewcompress.BorderRadius = 0;
            this.btn_viewcompress.ButtonText = "Compress";
            this.btn_viewcompress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_viewcompress.DisabledColor = System.Drawing.Color.Gray;
            this.btn_viewcompress.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_viewcompress.Iconimage = null;
            this.btn_viewcompress.Iconimage_right = null;
            this.btn_viewcompress.Iconimage_right_Selected = null;
            this.btn_viewcompress.Iconimage_Selected = null;
            this.btn_viewcompress.IconMarginLeft = 0;
            this.btn_viewcompress.IconMarginRight = 0;
            this.btn_viewcompress.IconRightVisible = true;
            this.btn_viewcompress.IconRightZoom = 0D;
            this.btn_viewcompress.IconVisible = true;
            this.btn_viewcompress.IconZoom = 90D;
            this.btn_viewcompress.IsTab = false;
            this.btn_viewcompress.Location = new System.Drawing.Point(-148, 73);
            this.btn_viewcompress.Name = "btn_viewcompress";
            this.btn_viewcompress.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(63)))), ((int)(((byte)(102)))));
            this.btn_viewcompress.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(103)))), ((int)(((byte)(158)))));
            this.btn_viewcompress.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_viewcompress.selected = false;
            this.btn_viewcompress.Size = new System.Drawing.Size(190, 48);
            this.btn_viewcompress.TabIndex = 1;
            this.btn_viewcompress.Text = "Compress";
            this.btn_viewcompress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_viewcompress.Textcolor = System.Drawing.Color.White;
            this.btn_viewcompress.TextFont = new System.Drawing.Font("29LT Bukra Regular", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_viewcompress.Click += new System.EventHandler(this.btn_viewcompress_Click);
            // 
            // btn_slidemenu
            // 
            this.btn_slidemenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_slidemenu.BackColor = System.Drawing.Color.Transparent;
            this.btn_slidemenu.Image = ((System.Drawing.Image)(resources.GetObject("btn_slidemenu.Image")));
            this.btn_slidemenu.ImageActive = null;
            this.btn_slidemenu.Location = new System.Drawing.Point(6, 12);
            this.btn_slidemenu.Name = "btn_slidemenu";
            this.btn_slidemenu.Size = new System.Drawing.Size(30, 30);
            this.btn_slidemenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_slidemenu.TabIndex = 0;
            this.btn_slidemenu.TabStop = false;
            this.btn_slidemenu.Zoom = 10;
            this.btn_slidemenu.Click += new System.EventHandler(this.btn_slidemenu_Click);
            // 
            // pnl_container
            // 
            this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_container.Location = new System.Drawing.Point(42, 48);
            this.pnl_container.Name = "pnl_container";
            this.pnl_container.Size = new System.Drawing.Size(940, 604);
            this.pnl_container.TabIndex = 4;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnl_toolbar;
            this.bunifuDragControl1.Vertical = true;
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 652);
            this.Controls.Add(this.pnl_container);
            this.Controls.Add(this.pnl_toolbar);
            this.Controls.Add(this.pnl_slide);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainView";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainView_Load);
            this.pnl_toolbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.pnl_slide.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_slidemenu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel pnl_toolbar;
        private Bunifu.Framework.UI.BunifuImageButton btn_min;
        private Bunifu.Framework.UI.BunifuImageButton btn_max;
        private Bunifu.Framework.UI.BunifuImageButton btn_close;
        private System.Windows.Forms.Panel pnl_slide;
        private Bunifu.Framework.UI.BunifuFlatButton btn_viewdecompress;
        private Bunifu.Framework.UI.BunifuFlatButton btn_viewcompress;
        private Bunifu.Framework.UI.BunifuImageButton btn_slidemenu;
        private System.Windows.Forms.Panel pnl_container;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuFlatButton btn_viewer;
    }
}