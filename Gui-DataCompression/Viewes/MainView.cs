﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gui_DataCompression.ParitialViews;



namespace Gui_DataCompression.Viewes
{
    public partial class MainView : Form
    {
        CompressParitialView compressView;
        DecompressParitialView decompressView;
        ImageViewerParitialView imageView;



        public MainView()
        {
            InitializeComponent();           
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            compressView = new CompressParitialView();
            decompressView = new DecompressParitialView();
            imageView = new ImageViewerParitialView();

            compressView.Visible = false;
            decompressView.Visible = false;
            imageView.Visible = false;

            pnl_container.Controls.Add(compressView);
            pnl_container.Controls.Add(decompressView);
            pnl_container.Controls.Add(imageView);

            compressView.Dock = DockStyle.Fill;
            decompressView.Dock = DockStyle.Fill;
            imageView.Dock = DockStyle.Fill;


            btn_viewcompress_Click(null, null);
        }

        private void btn_slidemenu_Click(object sender, EventArgs e)
        {
            if (pnl_slide.Width == 190)
            {
                pnl_slide.Width = 42;
            }
            else
            {
                pnl_slide.Width = 190;
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            try
            {
                Environment.Exit(1);
            }
            catch
            {

            }
        }

        private void btn_max_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void btn_min_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btn_viewcompress_Click(object sender, EventArgs e)
        {
            HideAll(compressView);
        }

        private void btn_viewdecompress_Click(object sender, EventArgs e)
        {
            HideAll(decompressView);
        }

        public void HideAll(Control justThis)
        {
            compressView.Visible = false;
            decompressView.Visible = false;
            justThis.Visible = true;
            pnl_toolbar.Focus();
        }

        private void pnl_toolbar_DoubleClick(object sender, EventArgs e)
        {
            if(this.WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
            else
                this.WindowState = FormWindowState.Normal;
        }

        private void btn_viewer_Click(object sender, EventArgs e)
        {
            HideAll(imageView);
        }
    }
}
