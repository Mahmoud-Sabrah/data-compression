﻿using CompressAlgorithms.Delegates;
using CompressAlgorithms.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompressAlgorithms.Lossless.Lzw
{
   public class lzwAlgo : ILosslessAlgorithm
    {



        public bool Compress(string inputFileName, string outputFileName)
        {
            //fill the dictonary with extened ascii values
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            for (int i = 0; i < 256; i++)
                dictionary.Add(((char)i).ToString(), i);



            FileStream fileToCompress = new FileStream(inputFileName, FileMode.Open);
            FileStream outputFile = new FileStream(outputFileName, FileMode.Create);


            BinaryReader reader = new BinaryReader(fileToCompress);
            BinaryWriter writer = new BinaryWriter(outputFile);


            string w = string.Empty;
            List<int> compressed = new List<int>();

            byte[] buffer = new byte[4096];





            int readit = 0;
            char c;
            try
            {
                while ((readit = reader.Read(buffer,0,4096)) != 0)
                {
                    for (int i = 0; i < readit; i++)
                    {
                        c = (char)buffer[i];
                        string wc = w + c;

                        if (dictionary.ContainsKey(wc))
                        {
                            w = wc;
                        }
                        else
                        {
                            //write to output
                            writer.Write(BitConverter.GetBytes(dictionary[w]), 0, 2);
                            //bufferToWrite.AddRange(BitConverter.GetBytes(dictionary[w]), 0, 2);

                            if (dictionary.Count < 65536) //because we are using 16 bits  as dictionary table 
                            {
                                // wc is a new sequence; add it to the dictionary
                                dictionary.Add(wc, dictionary.Count);
                            }

                            w = c.ToString();
                        }
                    }
                }




            }
            catch (EndOfStreamException ex)
            {
               
            }


            // write remaining output if necessary
            if (!string.IsNullOrEmpty(w))
                writer.Write(BitConverter.GetBytes(dictionary[w]), 0, 2);

            reader.Close();
            fileToCompress.Close();


            writer.Close();
            outputFile.Close();

            return true;
        }

        public bool Decompress(string inputFilePath, string outputPath)
        {


            FileStream fileToDecompress = new FileStream(inputFilePath, FileMode.Open);
            //FileStream outputFile = new FileStream(Path.GetFullPath(outputPath) + "\\" + Path.GetFileNameWithoutExtension(inputFilePath), FileMode.Create);


            BinaryReader reader = new BinaryReader(fileToDecompress);
            //BinaryWriter writer = new BinaryWriter(outputFile,Encoding.ASCII);





            // build the dictionary
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            for (int i = 0; i < 256; i++)
                dictionary.Add(i, ((char)i).ToString());




            int sss = reader.ReadUInt16();
            string w = dictionary[sss];
            StringBuilder decompressed = new StringBuilder(w);


            int k;
            try
            {
                //writer.Write(w);
                while (true)
                {
                    k = reader.ReadUInt16();
                    string entry = null;
                    if (dictionary.ContainsKey(k))
                        entry = dictionary[k];
                    else if (k == dictionary.Count)
                        entry = w + w[0];

                    //writer.Write(entry);
                    decompressed.Append(entry);

                    if (dictionary.Count < 65536) //because we are using 16 bits  as dictionary table & as charaters encode 
                    {
                        // new sequence; add it to the dictionary
                        dictionary.Add(dictionary.Count, w + entry[0]);
                    }


                    w = entry;
                }
            }
            catch (EndOfStreamException ex)
            {
                Console.WriteLine("sdas");
            }


            //writer.Close();

            List<byte> decOutput = new List<byte>();
            foreach (var item in decompressed.ToString())
            {
                decOutput.Add((byte)item);
            }
            byte[] byText = decOutput.ToArray();
            File.WriteAllBytes(outputPath, byText);

            reader.Close();
            fileToDecompress.Close();

            return true;
        }
    }
}
