﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CompressAlgorithms.Delegates;


namespace CompressAlgorithms.Interfaces
{
    interface ILosslessAlgorithm
    {
        bool Compress(string inputFileName, string outputFileName);

        bool Decompress(string inputFileName, string outputFileName);

    }
}
